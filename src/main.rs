use std::collections::HashSet;
use std::collections::HashMap;
use std::hash::Hash;
use std::hash::Hasher;
use std::option::Option;
use rand::Rng;

#[derive(Copy, Clone, PartialEq, Hash, Eq)]
enum Neighbor {
    North,
    South,
    East,
    West
}

#[derive(Copy, Clone)]
struct Cell {
    row: usize,
    column: usize,
}

impl Hash for Cell {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.row.hash(state);
        self.column.hash(state);
    }
}

impl PartialEq for Cell {
    fn eq(&self, other: &Cell) -> bool {
        self.row == other.row && self.column == other.column
    }
}

impl Eq for Cell {}

impl Cell {
    pub fn new(r: usize, c: usize) -> Cell {
        Cell { row: r, column: c }
    }
}

#[test]
fn test_create_cell() {
    let c = Cell::new(1, 1);

    assert_eq!(c.row, 1);
    assert_eq!(c.column, 1);
}

struct Grid {
    rows: usize,
    columns: usize,

    links: HashMap<Cell, HashSet<Cell>>,
    neighbors: HashMap<Cell, HashSet<Neighbor>>,
}

impl Grid {
    pub fn new(rows: usize, columns: usize) -> Grid {
        let mut links = HashMap::<Cell, HashSet<Cell>>::new();
        let mut neighbors = HashMap::<Cell, HashSet<Neighbor>>::new();

        for i in 0..rows {
            for j in 0..columns {
                let cell =  Cell::new(i, j);
                links.insert(cell, HashSet::<Cell>::new());

                let mut neighbors_set = HashSet::<Neighbor>::new();

                if cell.row > 0 {
                    neighbors_set.insert(Neighbor::North);
                }

                if cell.row < rows - 1 {
                    neighbors_set.insert(Neighbor::South);
                }

                if cell.column > 0 {
                    neighbors_set.insert(Neighbor::West);
                }

                if cell.column < columns - 1 {
                    neighbors_set.insert(Neighbor::East);
                }

                neighbors.insert(cell, neighbors_set);
            }
        }

        Grid {rows: rows, columns: columns, links: links, neighbors: neighbors }
    }

    fn is_inside(row: usize, col: usize, rows: usize, cols: usize) -> bool {
        (row < rows) && (col < cols)
    }

    pub fn has_neighbor(&self, neighbor: Neighbor, cell: &Cell) -> bool {
        self.neighbors[cell].contains(&neighbor)
    }

    fn link(&mut self, a: &Cell, b: Cell) {
        self.links.get_mut(a).unwrap().insert(b);
    }

    fn unlink(&mut self, a: &Cell, b: &Cell) {
        self.links.get_mut(a).unwrap().remove(b);
    }

    fn linked(&self, a: &Cell, b: &Cell) -> bool {
        self.links[a].contains(b)
    }

    pub fn get_cell(&self, row: usize, column: usize) -> Option<Cell> {
        if  Grid::is_inside(row, column, self.rows, self.columns) {
            Some(Cell { row: row, column: column })
        } else {
            None
        }
    }

    pub fn neighbor(&self, n: Neighbor, cell: &Cell) -> Option<Cell> {
        if !self.has_neighbor(n, &cell) {
            None
        } else {
            match n {
                Neighbor::North => Some(Cell::new(cell.row - 1, cell.column)),
                Neighbor::South => Some(Cell::new(cell.row + 1, cell.column)),
                Neighbor::East => Some(Cell::new(cell.row, cell.column + 1)),
                Neighbor::West => Some(Cell::new(cell.row, cell.column - 1)),
            }
        }
    }
}

#[test]
fn test_grid_get_cell() -> std::result::Result<(), String> {
    let grid = Grid::new(4, 4);

    let c = grid.get_cell(0, 0);
    match c {
        Some(_) => Ok(()),
        None => Err(String::from("get_cell failed ")),
    }
}

#[test]
fn test_has_some_neighbors() {
    let grid = Grid::new(4, 4);
    let c = grid.get_cell(0, 0).unwrap();

    assert!(!grid.has_neighbor(Neighbor::North, &c));
    assert!(grid.has_neighbor(Neighbor::South, &c));
    assert!(grid.has_neighbor(Neighbor::East, &c));
    assert!(!grid.has_neighbor(Neighbor::West, &c));

    let d = grid.get_cell(3, 3).unwrap();

    assert!(grid.has_neighbor(Neighbor::North, &d));
    assert!(!grid.has_neighbor(Neighbor::South, &d));
    assert!(!grid.has_neighbor(Neighbor::East, &d));
    assert!(grid.has_neighbor(Neighbor::West, &d));
}

#[test]
fn test_has_all_neighbors() {
    let grid = Grid::new(4, 4);
    let c = grid.get_cell(1, 1).unwrap();

    assert!(grid.has_neighbor(Neighbor::North, &c));
    assert!(grid.has_neighbor(Neighbor::South, &c));
    assert!(grid.has_neighbor(Neighbor::East, &c));
    assert!(grid.has_neighbor(Neighbor::West, &c));
}

#[test]
fn test_linking() {
    let mut grid = Grid::new(4, 4);

    let a = grid.get_cell(0, 0).unwrap();
    let b = grid.get_cell(1, 0).unwrap();

    grid.link(&a, b);

    assert!(grid.linked(&a, &b));
    assert!(!grid.linked(&b, &a));

    grid.unlink(&a, &b);
    assert!(!grid.linked(&a, &b));
}

fn calculate_binary_tree_maze(grid: &mut Grid) {
    let mut rng = rand::thread_rng();

    for r in 0..grid.rows {
        for c in 0..grid.columns {
            let mut neighbors = Vec::<Neighbor>::new();
            let cell = grid.get_cell(r, c).unwrap();

            if grid.has_neighbor(Neighbor::South, &cell) {
                neighbors.push(Neighbor::South);
            }

            if grid.has_neighbor(Neighbor::East, &cell) {
                neighbors.push(Neighbor::East);
            }

            if !neighbors.is_empty() {
                let n: usize = rng.gen_range(0..neighbors.len());
                let neighbor = grid.neighbor(neighbors[n], &cell);
                grid.link(&cell, neighbor.unwrap());
            }
        }
    }
}

fn draw_boundary(neighbor: Neighbor, grid: &Grid, cell: &Cell, a: &str, b: &str) -> String {
    let neighbor_cell = grid.neighbor(neighbor, cell);

    match neighbor_cell {
        Some(c) => if grid.linked(&cell, &c) {
            a.to_string()
        } else {
            b.to_string()
        },
        None => b.to_string(),
    }
}

fn grid_to_string(grid: &Grid) -> String{
    let mut output: String = "+".to_string();
    for _i in 0..grid.columns {
        output += &"---+".to_string();
    }
    output += "\n";

    for r in 0..grid.rows {
        let mut top: String = "|".to_string();
        let mut bottom: String = "+".to_string();

        for c in 0..grid.columns {
            let cell = grid.get_cell(r, c).unwrap();

            let east_boundary = draw_boundary(Neighbor::East, &grid, &cell, " ", "|");
            let body = "   ".to_string();
            top += &(body + &east_boundary);

            let south_boundary = draw_boundary(Neighbor::South, &grid, &cell, "   ", "---");
            let corner = "+";
            bottom += &(south_boundary + corner);
        }

        output += &(top + "\n");
        output += &(bottom + "\n");
    }

    output
}

fn main() {
    let mut grid = Grid::new(5, 5);

    let output1 = grid_to_string(&grid);
    println!("{}", output1);

    calculate_binary_tree_maze(&mut grid);
    let output2 = grid_to_string(&grid);
    println!("{}", output2);
}
